// classes of the things to be display on the canvas
$(document).ready( () => {
    class Shape
    {
        constructor(size, color)
        {
            this.size = size;
            this.color = color;
        }
    }

    class Img
    {
        constructor(img)
        {
            this.img = img;
        }

        draw(ctx)
        {
            ctx.drawImage(this.img, 0, 0);
        }
    }
    
    class Textbox extends Shape
    {
        constructor(font, startPoint)
        {
            super(font.size, font.color);
            this.startPoint = startPoint;
            this.typeface = font.typeface;
            this.content = "";
            this.editting = true;
    
            $(document).keydown((event)=>{
                if(this.editting)
                {
                    this.handleInput(event.which);
                }
            });
        }
    
        handleInput(keycode)
        {
            switch(keycode)
            {
                case 8:         // backspace
                if(0 < this.content.length)
                {
                    this.content = this.content.slice(0, this.content.length - 1);
                }
                break;
                case 13:        // enter
                this.editting = false;
                break;
                default:
                this.content += String.fromCharCode(keycode);
            }
            redraw();
        }
    
        draw(ctx)
        {    
            ctx.beginPath();
            ctx.font = `${this.size}px ${this.typeface}`;
            ctx.fillStyle = this.color;
            ctx.textAlign = "left";
            console.log(`${this.size}px ${this.typeface}`);
    
            ctx.fillText(this.content, this.startPoint.x, this.startPoint.y);
        }
    }
    
    class Circle extends Shape
    {
        constructor(size, color, startPoint)
        {
            super(size, color);
            this.startPoint = startPoint;
            this.endPoint = startPoint;
        }
    
        draw(ctx)
        {
            let centerPoint = {
                x: (this.endPoint.x + this.startPoint.x)/2,
                y: (this.startPoint.y + this.endPoint.y)/2
            };
            let radius = Math.max(Math.abs(this.startPoint.x - this.endPoint.x), Math.abs(this.startPoint.y - this.endPoint.y))/1.414/2;
                
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.size;
    
            ctx.arc(centerPoint.x, centerPoint.y, radius, 0, 2 * Math.PI, false);
    
            ctx.stroke();
            ctx.closePath();
        }
    }
    
    
    class Tri extends Shape
    {
        constructor(size, color, startPoint)
        {
            super(size, color);
            this.startPoint = startPoint;
            this.endPoint = startPoint;
        }
    
        draw(ctx)
        {
            let topVertex = {
                x: (this.endPoint.x + this.startPoint.x)/2,
                y: this.startPoint.y
            };
            let leftDownVertex = {
                x: this.startPoint.x,
                y: this.endPoint.y
            };
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.size;
    
            ctx.moveTo(topVertex.x, topVertex.y);
            ctx.lineTo(leftDownVertex.x, leftDownVertex.y);
            ctx.lineTo(this.endPoint.x, this.endPoint.y);
            ctx.lineTo(topVertex.x, topVertex.y);
    
            ctx.stroke();
            ctx.closePath();
        }
    }
    
    class Rect extends Shape
    {
        constructor(size, color, startPoint)
        {
            super(size, color);
            this.startPoint = startPoint;
            this.endPoint = startPoint;
        }
    
        draw(ctx)
        {
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.size;
    
            ctx.moveTo(this.startPoint.x, this.startPoint.y);
            ctx.lineTo(this.endPoint.x, this.startPoint.y);
            ctx.lineTo(this.endPoint.x, this.endPoint.y);
            ctx.lineTo(this.startPoint.x, this.endPoint.y);
            ctx.lineTo(this.startPoint.x, this.startPoint.y);
    
            ctx.stroke();
            ctx.closePath();
        }
    }
    
    class Line extends Shape
    {
        constructor(size, color, startPoint)
        {
            super(size, color);
            this.startPoint = startPoint;
            this.endPoint = startPoint;
        }
    
        draw(ctx)
        {
            ctx.beginPath();
            ctx.strokeStyle = this.color;
            ctx.lineWidth = this.size;
    
            ctx.moveTo(this.startPoint.x, this.startPoint.y);
            ctx.lineTo(this.endPoint.x, this.endPoint.y);
    
            ctx.stroke();
            ctx.closePath();
        }
    }
    
    class Path extends Shape
    {
        constructor(size, color, startPoint)
        {
            super(size, color);
            this.points = [startPoint];
        }
    
        draw(ctx)
        {
            if(0 < this.points.length)
            {
                ctx.beginPath();
                ctx.strokeStyle = this.color;
                ctx.lineWidth = this.size;
    
                ctx.moveTo(this.points[0].x, this.points[0].y);
                for(let i=1; i<this.points.length; i++)
                {
                    ctx.lineTo(this.points[i].x, this.points[i].y);
                    ctx.stroke();
                }
    
                ctx.closePath();
            }
        }
    }
    
    class Point
    {
        constructor(x, y)
        {
            this.x = x;
            this.y = y;
        }
    }
    var isDown = false;
    var canvas = document.getElementById("mainCanvas");
    var canvasRect = canvas.getBoundingClientRect();
    var ctx = canvas.getContext("2d");
    
    var shapesToDraw = [];
    var shapesToRedo = [];

    var tool = "brush";
    var cur_shape;
    var brush = {
        color: "#000000",
        size: 5
    };
    var eraser = {
        color: "#FFFFFF",
        size: 5
    };
    var text = {
        color: brush.color,
        size: 20,
        typeface: "Arial"
    }
    var mouse = new Point(0, 0);

    // DEBUG //

    // TOOL BOX //
    $('#painterColorpicker').colpick({
        onSubmit:function(hsb,hex,rgb,el,bySetColor) {
            $(el).val('#'+hex);
            $(el).colpickHide();
            brush.color = '#'+hex;
            text.color = brush.color;
        }
    });
    $('#eraserColorpicker').colpick({
        onSubmit:function(hsb,hex,rgb,el,bySetColor) {
            $(el).val('#'+hex);
            $(el).colpickHide();
            eraser.color = '#'+hex;
        }
    });
    $("#SizePicker").change(()=>{ 
        switch(tool)
        {
            case "brush":
            brush.size = $("#SizePicker").val(); 
            break;
            case "eraser":
            eraser.size = $("#SizePicker").val(); 
            break;
            case "text":
            text.size = $("#SizePicker").val(); 
            break;
        }
    });

    // tools
    $("#brushButton").click(()=>{ 
        tool = "brush"; 
        canvas.style.cursor = "cell";
        var slider = document.getElementById("SizePicker");
        slider.min = 1;
        slider.max = 10;
        slider.value = brush.size;
    });
    $("#eraserButton").click(()=>{ 
        tool = "eraser"; 
        canvas.style.cursor = "cell";
        var slider = document.getElementById("SizePicker");
        slider.min = 1;
        slider.max = 10;
        slider.value = eraser.size;
    });
    $("#textButton").click(()=>{ 
        tool = "text"; 
        canvas.style.cursor = "text";
        var slider = document.getElementById("SizePicker");
        slider.min = 10;
        slider.max = 50;
        slider.value = text.size;
        console.log(`slider value = ${slider.value}`)
        console.log(`text size = ${text.size}`)
    });
    $("#lineButton").click(()=>{ tool = "line"; canvas.style.cursor = "crosshair";});
    $("#rectButton").click(()=>{ tool = "rect"; canvas.style.cursor = "crosshair";});
    $("#triButton").click(()=>{ tool = "tri"; canvas.style.cursor = "crosshair";});
    $("#circleButton").click(()=>{ tool = "circle"; canvas.style.cursor = "crosshair";});

    // typeface
    $("#typefaceSelector").change(()=>{
        text.typeface = $("#typefaceSelector").val();
    });

    // below tool
    $("#clearButton").click(()=>{
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        shapesToDraw = [];
        shapesToRedo = [];
    });
    $("#undoButton").click(()=>{
        if(0 < shapesToDraw.length)
        {
            shapesToRedo.push(shapesToDraw.pop());
            redraw();
        }
    });

    $("#redoButton").click(()=>{
        if(0 < shapesToRedo.length)
        {
            shapesToDraw.push(shapesToRedo.pop());
            redraw();
        }
    });
    $("#saveButton").click(()=>{
        console.log("save button clicked");
        var image = canvas.toDataURL("image/png");
        var link = document.createElement('a');
        link.href = image;
        link.download = "yourCanvas.png";
        link.click();
    });
    $("#imageUploader").change((e)=>{
        var reader = new FileReader();
        reader.onload = function(event){
            var img = new Image();
            img.onload = function(){
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage(img,0,0);
                shapesToDraw.push(new Img(img));
            }
            img.src = event.target.result;
        }
        reader.readAsDataURL(e.target.files[0]);   
    });


    // mouse events
    $("#mainCanvas").mousedown((event)=>{
        isDown = true;
        shapesToRedo = [];
        if(cur_shape instanceof Textbox && cur_shape.editting === true)
            cur_shape.editting = false;
        switch(tool)
        {
            case "brush":
            cur_shape = new Path(brush.size, brush.color, new Point(mouse.x, mouse.y));
            break;
            case "eraser":
            cur_shape = new Path(eraser.size, eraser.color, new Point(mouse.x, mouse.y));
            break;
            case "line":
            cur_shape = new Line(brush.size, brush.color, new Point(mouse.x, mouse.y));
            break;
            case "rect":
            cur_shape = new Rect(brush.size, brush.color, new Point(mouse.x, mouse.y));
            break;
            case "tri":
            cur_shape = new Tri(brush.size, brush.color, new Point(mouse.x, mouse.y));
            break;
            case "circle":
            cur_shape = new Circle(brush.size, brush.color, new Point(mouse.x, mouse.y));
            break;
            case "text":
            cur_shape = new Textbox(Object.create(text), new Point(mouse.x, mouse.y));
            break;
        }
        shapesToDraw.push(cur_shape);
    })
    $("#mainCanvas").mousemove((event)=>{
        mouse.x = event.clientX - canvasRect.left;
        mouse.y = event.clientY - canvasRect.top;
        
        if(isDown)
        {
            switch(tool)
            {
                case "brush":
                cur_shape.points.push(new Point(mouse.x, mouse.y));
                break;
                case "eraser":
                cur_shape.points.push(new Point(mouse.x, mouse.y));
                break;
                case "line":
                cur_shape.endPoint = mouse;
                break;
                case "rect":
                cur_shape.endPoint = mouse;
                break;
                case "tri":
                cur_shape.endPoint = mouse;
                break;
                case "circle":
                cur_shape.endPoint = mouse;
                break;
            }
            
            redraw();
        }
    })
    $("#mainCanvas").mouseup(()=>{
        isDown = false;

        switch(tool)
        {
            case "line":
            cur_shape.endPoint = new Point(mouse.x, mouse.y);
            break;
            case "rect":
            cur_shape.endPoint = new Point(mouse.x, mouse.y);
            break;
            case "tri":
            cur_shape.endPoint = new Point(mouse.x, mouse.y);
            break;
            case "circle":
            cur_shape.endPoint = new Point(mouse.x, mouse.y);
            break;
        }
    })
    $("#mainCanvas").mouseout(()=>{ 
        isDown = false;
    })

    function redraw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        shapesToDraw.forEach(shape => {
            shape.draw(ctx);
        })
    };


});